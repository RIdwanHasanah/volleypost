package com.indonesia.ridwan.volleypost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    public static final String REGISTER_URL = "http://192.168.1.17/project/user/postUser.php";

    //Nama Databse di PHP mysql
    public static final String KEY_GENRE = "jenis_kelamin";
    public static final String KEY_NAME = "nama";
    public static final String KEY_ADDRESS = "alamat";
    public static final String KEY_PHONSEL = "ponsel";
    public static final String KEY_STATUSS = "status";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";

    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText ejk;
    private EditText enama;
    private EditText eponsel;
    private EditText ealamat;
    private EditText estatus;

    private Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextEmail= (EditText) findViewById(R.id.editTextEmail);
        ejk = (EditText) findViewById(R.id.jk);
        ealamat =(EditText) findViewById(R.id.alamat);
        enama = (EditText) findViewById(R.id.ename);
        eponsel = (EditText) findViewById(R.id.ponsel);
        estatus = (EditText) findViewById(R.id.status);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);

        buttonRegister.setOnClickListener(this);
    }

    private void registerUser(){
        final String password = editTextPassword.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        final String genre = ejk.getText().toString().trim();
        final String address = ealamat.getText().toString().trim();
        final String name = enama.getText().toString().trim();
        final String phonsel = eponsel.getText().toString().trim();
        final String statuss = estatus.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("data",response.toString());
                       /* Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*Toast.makeText(MainActivity.this,error.toString(), Toast.LENGTH_LONG).show();*/
                        Log.e("Error",error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(KEY_GENRE,genre);
                params.put(KEY_NAME,name);
                params.put(KEY_PHONSEL,phonsel);
                params.put(KEY_STATUSS,statuss);
                params.put(KEY_ADDRESS,address);
                params.put(KEY_PASSWORD,password);
                params.put(KEY_EMAIL, email);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonRegister) {
            registerUser();
        }

    }

}
